# Quick Start

In your plugin folder create a lib folder and clone the libirary sources files into that lib folder

```console
$ mkdir lib && cd lib

$ git clone https://gitlab.com/oracions.dev/zenidata-base-plugin-simple-core-lib.git
```

In this example we are going to create a table to store student. We will save the first, last name and the student's age.

## First Step: Create a custom WP_List_Table class

Import the base-wp-list-table.php file you lib folder.

```php
include 'lib/zenidata-base-plugin-simple-core-lib/core/base-wp-list-table.php';
```

In your plugin php file, create the Student_WP_List_Table who extends the wordpress Base_WP_List_Table

```php
class Student_WP_List_Table extends Base_WP_List_Table {
    public function __construct() {
		$args = [
			'singular' => __( 'Student', 'sp' ),
			'plural'   => __( 'Students', 'sp' ),
			'ajax'     => false,
		];

		$main_table_name = 'students';

		$columns = [
			'firstname'    => __( 'Firstname', 'sp' ),
			'lastname'    => __( 'Lastname', 'sp' ),
			'age'    => __( 'Age', 'sp' ),
			'updated_at' => __('Updated At', 'sp'),
		];

		$sortable_columns = [
			'firstname' => array( 'firstname', true ),
			'lastname' => array( 'lastname', true ),
			'age' => array( 'age', false )
		];

		$edit_page_slug = 'edit-student';

		Parent::__construct($args, $main_table_name, $columns, $sortable_columns, $edit_page_slug);

		$this->init();
		// $this->seed_main_table_data();
	}

    public function setup_main_table() {
		$table_name = self::$main_table_name;
		$sql = "
			firstname varchar(255) NOT NULL,
			lastname varchar(255) NOT NULL,
			age varchar(255) NOT NULL,
		";
		$this->create_table($table_name, $sql);
	}

    public function seed_main_table_data()
    {
      global $wpdb;

      for ($i = 1; $i < 11; ++$i) {
        $data = array(
          "firstname" => "Firstname " . $i,
          "lastname" => "Lastname " .$i,
          "age" => 2 * $i,
        );
        $wpdb->insert(self::$prefixed_main_table_name, $data);
      }
    }

    public function column_firstname($item) {
		return $this->__custom__main_column($item, 'firstname');
	}
}
```

## Second Step: Create a custom SP_Plugin class

create the Students_SP_Plugin who extends the wordpress Base_SP_Plugin

```php
class Students_SP_Plugin extends Base_SP_Plugin {

    public function __construct() {
        $this->main_page_title = 'Students';
		$this->main_page_slug = 'students-list';
		$this->main_page_menu = 'Students';
		$this->main_page_menu_icon = null; // 'dashicons-editor-unlink';
		$this->new_item_page_slug = 'new-student';

		$this->edit_item_page_slug = 'edit-student';

		Parent::__construct(
			$this->new_item_page_slug,
			$this->main_page_title,
			$this->main_page_menu,
			$this->main_page_slug,
			$this->main_page_menu_icon,
		);

		$this->add_pages();
	}

    public function screen_option() {

		$option = 'per_page';
		$args   = [
			'label'   => 'Students',
			'default' => 5,
			'option'  => 'students_per_page'
		];

		add_screen_option( $option, $args );

		$this->item_obj = new Student_WP_List_Table();
	}

	public function add_item_template() {
		?>
			<h1>Add a new Student</h2>
		<?php
	}

	public function add_pages() {
		add_action('admin_menu', function() {
			add_submenu_page(
				'',
				'Edit',
				'Edit ',
				'manage_options',
				$this->edit_item_page_slug,
				function() {
					?>
						<h1>Edit the student</h1>
					<?php
				}
			);
		});
	}

    public static function get_instance() {
		// if ( true || ! isset( self::$instance ) ) {
			self::$instance = new self();
		// }
		return self::$instance;
	}
}
```

## Final Step:

Load the instance of Students_SP_Plugin in the hook plugin_loaded

```php
add_action('plugins_loaded', function () {
    Students_SP_Plugin::get_instance();
});
```

If everythis is fine, you should get this error:

![image](https://gitlab.com/oracions.dev/zenidata-base-plugin-simple/-/raw/master/screeshot/table-error.png?ref_type=heads)

This is because the table students does'nt exist yet in the database. To create that table just uncomment the code in the contructor of your custom Student_WP_List_Table.

```php
$this->init();
$this->setup_tables();
// $this->seed_main_table_data();
```

The setup_tables function create the table and the seed_main_table_data function create some fake data into that table.
After that, everything sould work correctly and you should have a screen like this:

![image](https://gitlab.com/oracions.dev/zenidata-base-plugin-simple/-/raw/master/screeshot/all-done.png?ref_type=heads)
