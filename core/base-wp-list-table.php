<?php
if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Base_WP_List_Table extends WP_List_Table {
	public static $main_table_name;
	public static $prefixed_main_table_name;

	public $__custom__columns;
	public $__custom__sortable_columns;
	public $edit_page_slug;

	/** Class constructor */
	public function __construct(
		$args = null,
		$main_table_name = null,
		$columns = null,
		$sortable_columns = null,
		$edit_page_slug = null,
	) {
		global $wpdb;
		self::$main_table_name = $main_table_name ? $main_table_name : 'items';
		self::$prefixed_main_table_name = $wpdb->base_prefix . $main_table_name;
		
		$this->__custom__columns = $columns;
		$this->__custom__sortable_columns = $sortable_columns;
		$this->edit_page_slug = $edit_page_slug;
		
		$defaultArgs = [
			'singular' => __( 'item', 'sp' ),
			'plural'   => __( 'items', 'sp' ),
			'ajax'     => false
		];
		parent::__construct($args ? $args : $defaultArgs);

        // $this->init();
	}

	public function init() {
        if (!$this->table_exists(self::$main_table_name)) {
            $this->setup_main_table();
        }
		
		// $this->seed_data();
	}

	public function table_exists($table_name) {
		global $wpdb;
		$prefixed_table_name = $wpdb->base_prefix . $table_name;
		$query = $wpdb->prepare(
            'SHOW TABLES LIKE %s',
            $wpdb->esc_like($prefixed_table_name)
        );
        if (!$wpdb->get_var($query) == $prefixed_table_name) {
            return false;
        }
		return true;
	}

	public function setup_main_table() {
		return;
		$table_name = self::$main_table_name;
		$sql = "
			name varchar(255) NOT NULL,
			address varchar(255) NULL,
			city varchar(255) NULL,
		";
		$this->create_table($table_name, $sql); 
	}

    public function create_table($table_name, $sql) {
        global $wpdb;
        // $table_name = 'items';
        $charset_collate = $wpdb->get_charset_collate();
		$prefixed_table_name = $wpdb->base_prefix . $table_name;
		
		// $table_name = self::$prefixed_main_table_name;

		// name varchar(255) NOT NULL,
		// address varchar(255) NULL,
		// city varchar(255) NULL,
        $sql = "CREATE TABLE $prefixed_table_name (
            ID INT NOT NULL AUTO_INCREMENT,
      
            $sql
            
            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
            updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

            PRIMARY KEY  (ID)
        ) $charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }

    function seed_data()
    {
      global $wpdb;
  
      for ($i = 1; $i < 11; ++$i) {
        $data = array(
          "name" => "Name N° " . $i,
          "city" => "City N° " .$i,
          "address" => "Address N° " .$i,
        );
        $wpdb->insert($main_table, $data);
      }
    }


	/**
	 * Retrieve items data from the database
	 *
	 * @param int $per_page
	 * @param int $page_number
	 *
	 * @return mixed
	 */
	public static function get_items( $per_page = 5, $page_number = 1 ) {
		global $wpdb;
		$table = self::$prefixed_main_table_name;

		$sql = "SELECT * FROM $table";

		if ( ! empty( $_REQUEST['orderby'] ) ) {
			$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
		}

		$sql .= " LIMIT $per_page";
		$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		return $result;
	}

	// public static function __custom__get_items( $table_name, $per_page = 5, $page_number = 1 ) {
	// 	global $wpdb;
	// 	$prefixed_table_name = $wpdb->base_prefix . $table_name;

	// 	$sql = "SELECT * FROM $prefixed_table_name";

	// 	if ( ! empty( $_REQUEST['orderby'] ) ) {
	// 		$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
	// 		$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
	// 	}

	// 	$sql .= " LIMIT $per_page";
	// 	$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;

	// 	$result = $wpdb->get_results( $sql, 'ARRAY_A' );

	// 	return $result;
	// }


	/**
	 * Delete a item record.
	 *
	 * @param int $id item ID
	 */
	public static function delete_item( $id ) {
		global $wpdb;
		$table_name = self::$prefixed_main_table_name;
		$wpdb->delete(
			$table_name,
			[ 'ID' => $id ],
			[ '%d' ]
		);
	}


	/**
	 * Returns the count of records in the database.
	 *
	 * @return null|string
	 */
	public static function record_count() {
		global $wpdb;

		$table = self::$prefixed_main_table_name;

		$sql = "SELECT COUNT(*) FROM $table";

		return $wpdb->get_var( $sql );
	}


	/** Text displayed when no item data is available */
	public function no_items() {
		_e( 'No '. self::$main_table_name .' avaliable.', 'sp' );
	}


	/**
	 * Render a column when no column specific method exist.
	 *
	 * @param array $item
	 * @param string $column_name
	 *
	 * @return mixed
	 */
	public function column_default( $item, $column_name ) {
		switch ( $column_name ) {
			// case 'address':
			// case 'city':
				// return $item[ $column_name ];
			default:
				return $item[ $column_name ];
				// return print_r( $item, true ); //Show the whole array for troubleshooting purposes
		}
	}

	/**
	 * Render the bulk edit checkbox
	 *
	 * @param array $item
	 *
	 * @return string
	 */
	function column_cb( $item ) {
		return sprintf(
			'<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['ID']
		);
	}


	/**
	 * Method for name column
	 *
	 * @param array $item an array of DB data
	 *
	 * @return string
	 */
	public function __custom__main_column( $item, $field ) {
		$delete_nonce = wp_create_nonce( 'sp_delete_item' );
		$edit_nonce = wp_create_nonce( 'sp_edit_item' );
		// $title = '<strong>' . $item['name'] . '</strong>';

		// $actions = [
		// 	'delete' => sprintf( '<a href="?page=%s&action=%s&item=%s&_wpnonce=%s">Delete</a>', esc_attr( $_REQUEST['page'] ), 'delete', absint( $item['ID'] ), $delete_nonce )
		// ];

		// return $title . $this->row_actions( $actions );

		// $item_json = json_decode(json_encode($item), true);
		$actions = array(
			'delete' => sprintf(
				'<a href="?page=%s&action=%s&item_id=%s&_wpnonce=%s">Delete</a>',
				esc_attr( $_REQUEST['page'] ),
				'delete',
				absint($item['ID']),
				$delete_nonce,
			),
		);


		if ($this->edit_page_slug) {
			array_unshift($actions, sprintf(
				'<a href="?page=%s&action=%s&item_id=%s_wpnonce=%s">Edit</a>',
				esc_attr($this->edit_page_slug),
				'edit',
				absint($item['ID']),
				$edit_nonce
			));
		}
		
		return sprintf('%s %s', $item[$field], $this->row_actions($actions));
	}

	/**
	 *  Associative array of columns
	 *
	 * @return array
	 */
	function get_columns() {
		$columns = [
			'cb'      => '<input type="checkbox" />',
		];

		$cols =  [
			'ID'    => __( 'ID', 'sp' ),
			'created_at'    => __( 'Created At', 'sp' ),
			'updated_at'    => __( 'Updated At', 'sp' ),
		];

		$output = $this->__custom__columns ? $this->__custom__columns : $cols;
		
		return array_merge($columns, $output);
	}


	/**
	 * Columns to make sortable.
	 *
	 * @return array
	 */
	public function get_sortable_columns() {
		$sortable_cols = array(
			'updated_at' => array( 'updated_at', true ),
			'created_at' => array( 'created_at', false )
		);

		$output = $this->__custom__sortable_columns ? $this->__custom__sortable_columns : $sortable_cols;

		return $output;
	}

	/**
	 * Returns an associative array containing the bulk action
	 *
	 * @return array
	 */
	public function get_bulk_actions() {
		$actions = [
			'bulk-delete' => 'Delete'
		];

		return $actions;
	}


	/**
	 * Handles data query and filter, sorting, and pagination.
	 */
	public function prepare_items() {

		$this->_column_headers = $this->get_column_info();

		/** Process bulk action */
		$this->process_bulk_action();

		$per_page     = $this->get_items_per_page( self::$main_table_name . '_per_page', 7 );
		$current_page = $this->get_pagenum();
		$total_items  = self::record_count();

		$this->set_pagination_args( [
			'total_items' => $total_items, //WE have to calculate the total number of items
			'per_page'    => $per_page //WE have to determine how many items to show on a page
		] );

		$method = 'get_items';
		$this->items = self::$method( $per_page, $current_page );
	}

	// public function __custom__prepare_items($method) {
	// 	die($method);
	// 	$this->_column_headers = $this->get_column_info();

	// 	/** Process bulk action */
	// 	$this->process_bulk_action();

	// 	$per_page     = $this->get_items_per_page( self::$main_table_name . '_per_page', 7 );
	// 	$current_page = $this->get_pagenum();
	// 	$total_items  = self::record_count();

	// 	$this->set_pagination_args( [
	// 		'total_items' => $total_items, //WE have to calculate the total number of items
	// 		'per_page'    => $per_page //WE have to determine how many items to show on a page
	// 	] );

	// 	// $this->items = self::get_items( $per_page, $current_page );
	// 	$this->items = self::$method( $per_page, $current_page );
	// }

	public function process_bulk_action() {

		//Detect when a bulk action is being triggered...
		if ( 'delete' === $this->current_action() ) {

			// In our file that handles the request, verify the nonce.
			$nonce = esc_attr( $_REQUEST['_wpnonce'] );

			if ( ! wp_verify_nonce( $nonce, 'sp_delete_item' ) ) {
				die( 'Go get a life script kiddies' );
			} else {
				$root = explode( '?', esc_url_raw( add_query_arg( array() ) ) )[0];
				$page = $_GET['page'];

				$url = $root . '?page=' . $page; 

				// die($url);

				self::delete_item( absint( $_GET['item_id'] ) );

		                // esc_url_raw() is used to prevent converting ampersand in url to "#038;"
		                // add_query_arg() return the current url
		                wp_redirect($url);
				exit;
			}

		}

		// If the delete bulk action is triggered
		if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-delete' )
		     || ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-delete' )
		) {

			$delete_ids = esc_sql( $_POST['bulk-delete'] );

			// loop over the array of record IDs and delete them
			foreach ( $delete_ids as $id ) {
				self::delete_item( $id );

			}

			// esc_url_raw() is used to prevent converting ampersand in url to "#038;"
		        // add_query_arg() return the current url
		        wp_redirect( esc_url_raw(add_query_arg()) );
			exit;
		}
	}
}

class Base_SP_Plugin {
	// class instance
	public static $instance;

	// item WP_List_Table object
	public $item_obj;

	// class constructor
	public function __construct(
		$new_item_page_slug,
		$main_page_title = null,
		$main_page_menu = null,
		$main_page_slug = null,
		$main_page_menu_icon = null,
	) {
		$this->new_item_page_url = get_admin_url() . "admin.php?page=$new_item_page_slug";
		$this->new_item_page_slug = $new_item_page_slug;
		$this->main_page_title = $main_page_title;
		$this->main_page_menu = $main_page_menu;
		$this->main_page_slug = $main_page_slug;
		$this->main_page_menu_icon = $main_page_menu_icon;
		// add_filter( 'set_screen_option', [ __CLASS__, 'set_screen' ], 10, 3 );
		// add_action( 'admin_menu', [ $this, 'plugin_menu' ] );

		add_action( 'admin_menu', function() {
			$hook = add_menu_page(
				$this->main_page_title ? $this->main_page_title : 'Example title',
				$this->main_page_menu ? "$this->main_page_menu" : 'Example menu',
				'manage_options',
				$this->main_page_slug ? $this->main_page_slug : 'example-slug',
				[ $this, 'plugin_settings_page' ],
				$this->main_page_menu_icon ? $this->main_page_menu_icon : 'dashicons-welcome-widgets-menus',
			);
			add_action( "load-$hook", [ $this, 'screen_option' ] );			
		});

		$this->add_item_page();
	}

	public function add_item_page() {
		add_action('admin_menu', function() {
			add_submenu_page(
				$this->main_page_slug,
				$this->main_page_title,
				"Add $this->main_page_title",
				'manage_options',
				$this->new_item_page_slug,
				[$this, 'add_item_template'],
			);
		});
	}

	public function add_item_template() {
	?>
		<div>
			<p>Extends the <strong>add_item_template</strong> to create the add item page</p>
		</div>
	<?php
	}

	// public static function set_screen( $status, $option, $value ) {
	// 	return $value;
	// }

	// public function plugin_menu() {

		
	// }


	/**
	 * Plugin settings page
	 */
	public function plugin_settings_page() {
		?>
		<div class="wrap">
			<h2>
				<?php echo $this->main_page_title  != null ?  $this->main_page_title : 'ZeniData List'; ?>
				<a href="<?php echo $this->new_item_page_url; ?>" class="page-title-action">Add New</a>
			</h2>

			<div id="poststuff">
				<div id="post-body" class="metabox-holder">
					<div id="post-body-content">
						<div class="meta-box-sortables ui-sortable">
							<form method="post">
								<?php
								$this->item_obj->prepare_items();
								$this->item_obj->display(); ?>
							</form>
						</div>
					</div>
				</div>
				<br class="clear">
			</div>
		</div>
	<?php
	}

	/**
	 * Screen options
	 */
	public function screen_option() {

		$option = 'per_page';
		$args   = [
			'label'   => 'items',
			'default' => 5,
			'option'  => 'items_per_page'
		];

		add_screen_option( $option, $args );

		$this->item_obj = new Base_WP_List_Table();
	}

	/** Singleton instance */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

}
